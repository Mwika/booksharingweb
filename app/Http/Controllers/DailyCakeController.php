<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Topic;
use App\Verse;
use App\Prayer;
use App\Testimony;
class DailyCakeController extends Controller
{
    public function cakeadmin(){
     $alltopics= DB::table('topics')->get();

     $allverses = DB::table('verses')->join('topics','topics.id', '=' ,'verses.topic_id')->select('verses.*','topics.topic_name')->get();

    

     $alltestimonies = DB::table('testimonies')->join('users','users.id', '=' ,'testimonies.Authuser_id')->select('testimonies.*','users.name')->get();



     $allprayers = DB::table('prayers')->get();

           
        return view('DailyCake.Admin')->with('alltopics',$alltopics)
        ->with('allverses',$allverses)
        ->with('alltestimonies',$alltestimonies)
        ->with('allprayers',$allprayers);
     

    }
    public function addtopic(Request $request){
    	$newtopic = new Topic();
    	$newtopic->topic_name = $request->topicname;
    	$newtopic->save();

    	return back();
    }

    public function addverse(Request $request){
      $newVerse =  new Verse();
      $newVerse->versefrom = $request->versefrom;
      $newVerse->content = $request->verse;
      $newVerse->topic_id = $request->topicid;

      $newVerse->save();

      return back();
    } 

    public function addTestimony(Request $request){
     $newTestimony = new Testimony();
     $newTestimony->testimony = $request->testimony;
     $newTestimony->Authuser_id= Auth::user()->name;
     $newTestimony->name= Auth::user()->name;
     $newTestimony->save();
     return back();
    }


    public function addprayer(Request $request){
    $newPrayer =  new Prayer();
    $newPrayer->prayer = $request->prayer;
    $newPrayer->prayerreference  = $request->prayerreference;
    $newPrayer->save();
    return back();
    }


public function deleteverse($id){
        $findverse =  Verse::find($id);
        $findverse->delete();
        return back();
}
public function deleteprayer($id){
     $findPrayer =  Prayer::find($id);
        $findPrayer->delete();
        return back();
}
public function deletetestimony($id){
     $findTestimony =  Testimony::find($id);
        $findTestimony->delete();
        return back();
}
public function deletetopic($id){
        $findTopic =  Topic::find($id);
        $findTopic->delete();
        return back();
}

}
