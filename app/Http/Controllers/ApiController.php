<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Subcategory;
use App\Comment;
use App\Link;
use DB;
use Response;
class ApiController extends Controller
{
    public function programmingbooks(){


   $programmingbooks= DB::table('books')->where('categoty_id','=',1)->get();
   
    
   
   	return Response::json($programmingbooks);

    }

    public function medicinebooks(){
       $medicinebooks= DB::table('books')->where('categoty_id','=',2)->get();

   
   
   	return Response::json($medicinebooks);
    }


    public function engineringbooks(){
       $engineringbooks= DB::table('books')->where('categoty_id','=',3)->get();
   	return Response::json($engineringbooks);
    }

    public function linkstobooks(){
       $linkstobooks= DB::table('books')->where('subcategories.id','=',4)->join('subcategories','books.subcategory_id','=','subcategories.id')->get();
   
   	return Response::json($linkstobooks); 
    }

    public function getcontent($id){
      $getbook = BooK::find($id);

       return Response::json($getbook);

    }
    public function getsubcategories($id){
        $subcategories = DB::table('subcategories')->where('categorynameid',$id)->select('subcategories.id','subcategories.subcategoryName')->get();
        return Response::json($subcategories);
    }

    public function addcomment(Request $request){
     $newcomment =  new Comment();
     $newcomment->comment = $request->bookcomment;
     $newcomment->bookid = $request->bookid;
     $newcomment->save();
      if( $newcomment->save()){
        $showcomments = DB::table('comments')->where('bookid',$request->bookid)->get();

        return Response::json($showcomments);
      }
 

    }

    public function addlink(Request $request){
      $newlink = new Link();
      $newlink->linktypename = $request->linkcategory;
      $newlink->linkdesription = $request->description;
      $newlink->linkname = $request->linkaddress;
      $newlink->save();
      if($newlink->save()){
        $alllinks = Link::all();
         return Response::json($alllinks);
      }
    }

public function getlinks(){
        $alllinks = Link::all();
         return Response::json($alllinks); 

}

public function allcomments($id){
  $showcomments = DB::table('comments')->where('bookid',$id)->get();

        return Response::json($showcomments);
}

}
