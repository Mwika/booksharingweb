<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailycakeTestimoner extends Model
{
    protected $fillable = ['name','uniquename'];
}
