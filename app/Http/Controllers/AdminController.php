<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\Book;
use Response;
use DB;
use File;
use Auth;
class AdminController extends Controller
{
   public function Addcategory(Request $request){
   	$newcategory  = new Category();
     $newcategory->categoryname = $request->category;
     $newcategory->save();
     if( $newcategory->save()){
     	return back();
     }

   }

   public function Addsubcategory(Request $request){
   	$newSubcategory = new Subcategory();
   	$newSubcategory->subcategoryName = $request->subcategory;
   	$newSubcategory->categorynameid = $request->category;
   	$newSubcategory->save();
   	   if( $newSubcategory->save()){
     	return back();
     }

   }
   public function adminpage(){
   	$allcategories = Category::all();
   	$subcategories = Subcategory::all();
   	$programmingcategory = Subcategory::all()->where('categorynameid',1);
 
   	$medicinecategory = Subcategory::all()->where('categorynameid',2);
  
   	$engineeringcategory = Subcategory::all()->where('categorynameid',3);
   	$linkscategory = Subcategory::all()->where('categorynameid',4);

    $programmingcategorybooks=Book::all()->where('categoty_id',1);
    $mediconecategorybooks=Book::all()->where('categoty_id',2);
    $engineeringcategorybooks=Book::all()->where('categoty_id',3);


   
   return view('Admin.index')->with('allcategories',$allcategories)->with('subcategories',$subcategories)->with('programmingcategory',$programmingcategory)->with('medicinecategory',$medicinecategory)->with('engineeringcategory',$engineeringcategory)->with('linkscategory',$linkscategory)->with('programmingcategorybooks',$programmingcategorybooks)->with('mediconecategorybooks',$mediconecategorybooks)->with('engineeringcategorybooks',$engineeringcategorybooks);
   }

        public function downloadfile($id){
          

          $downloadbook = Book::find($id)->bookname;
          
        
        return response()->download(base_path('public/Books/') .$downloadbook);

        }


     public function addbook(Request $request){
   
     $filebook = $request->file('book');
     $filepicture = $request->file('picture');
     $subcategory_id = $request->subcategoryName;

     $findcategory = $request->realcategoryid;
  
     $bookattachemnt = $request->file('book')->getClientOriginalName();
     $request->file('book')->move(base_path('public/Books'),$bookattachemnt);


     $pictureattachemnt = $request->file('picture')->getClientOriginalName();
     $request->file('picture')->move(base_path('public/Bookspictures'),$pictureattachemnt);
      
     $newbook=Book::create([
     'bookname' => $bookattachemnt,'picture' => $pictureattachemnt,'slug' => str_slug($bookattachemnt),
     'user_id' => 1,'categoty_id' => $findcategory, 'subcategory_id' => $subcategory_id,
     ]);    



     if($newbook){
      $allbooks =  DB::table('books')->get();
      return Response::json($allbooks);
     }

   }

   public function editbook($id){
    $findbook = Book::find($id);
      $programmingcategory = Subcategory::all()->where('categorynameid',1);
    return view('Admin.editprogrammingbooks')->with('findbook',$findbook)->with('programmingcategory',$programmingcategory);
   }
   public function editengineering($id){
    $findbook = Book::find($id);
      $engineering = Subcategory::all()->where('categorynameid',3);
    return view('Admin.editengineering')->with('findbook',$findbook)->with('engineering',$engineering);
   }
   public function editmedicinebook($id){
    $findbook = Book::find($id);
      $medicine = Subcategory::all()->where('categorynameid',2);
    return view('Admin.editmedicinebooks')->with('findbook',$findbook)->with('medicine',$medicine);
   }

   public function deletebook($id){
      $removebook = Book::find($id);
      File::delete('Bookspictures/'.$removebook->picture);
      File::delete('Books/'.$removebook->bookname);
      $removebook->delete();
      return back();
   }
   public function updateprogrammingbook(Request $request,$id){

    $updatebook = Book::find($id);
    $updatebook->subcategory_id = $request->subcategory_id;
    $updatebook->update();
    return back();

    
   }


}
