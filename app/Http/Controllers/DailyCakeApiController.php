<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Verse;
use App\Testimony;
use App\Prayer;
use App\DailycakeTestimoner;
use App\Topic;
use DB;
use Response;
class DailyCakeApiController extends Controller
{
   




    public function verses(){
        $aallverses = DB::table('verses')->join('topics','topics.id','=','verses.topic_id')
        ->select('verses.*','topics.topic_name')->get();

    	return Response::json($aallverses);

    }

    public function Testimonies(){
    $alltestimonies =  DB::table('testimonies')->get();

    return ($alltestimonies);
    }

	public function prayers(){
		$allprayers = DB::table('prayers')->get();
			return Response::json($allprayers);
	}

    public function adduserTestimony(Request $request){
     $newTestimony = new Testimony();
     $newTestimony->testimony = $request->testimony;
     $newTestimony->Authuser_id= $request->user;
     $newTestimony->name= $request->name;
     $newTestimony->save();

     if($newTestimony->save()){
            $testimony = DB::table('testimonies')->get();
             return Response::json($testimony);
        }
        else {
         return null;
        }

    }
        public function addusername(Request $request){
        $newuser = new DailycakeTestimoner();
        $newuser->name = $request->name;
        $idunique = base64_encode($request->name);
        $randmon = str_random(20);
        $add = $randmon.$idunique;

        $newuser->uniquename = $add;

        $newuser->save();
        if($newuser->save()){
            $user = DB::table('dailycake_testimoners')->where('uniquename',$add)->first();
             return Response::json($user);
        }
        else {
         return null;
        }


        }

        public function showtestimony($id){
            $testimonycontent = DB::table('testimonies')->where('id',$id)->first();
             return Response::json($testimonycontent);

        }

        public function showmytestimony($id){
            $mytestimonycontent = DB::table('testimonies')->where('Authuser_id',$id)->get();
             return Response::json($mytestimonycontent); 
        }

        public function todayverse(){

            $todayverse =  DB::table('verses')->join('topics','topics.id','=','verses.topic_id')->orderBy('verses.updated_at', 'desc')->first();
          
            return Response::json($todayverse);
        }

        public function testimonytoday(){
            $todaytestimony=  DB::table('testimonies')->orderBy('updated_at', 'desc')->first();
            return Response::json($todaytestimony);
        }
         public function prayertoday(){
           $todayprayer =  DB::table('prayers')->orderBy('updated_at', 'desc')->first();
            return Response::json($todayprayer);
        }

        public function changeusername(Request $request,$id){
            $name= $request->name;
             $saveuser= DB::table('dailycake_testimoners')->where('uniquename',$id)
             ->update(['name' => $name]);
        if($saveuser){
             $user = DB::table('dailycake_testimoners')->where('uniquename',$id)->first();
             return Response::json($user);
        }
       else {
            
       }

        }


}
