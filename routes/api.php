<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('all/books','ApiController@programmingbooks');
Route::get('all/medicinebooks','ApiController@medicinebooks');
Route::get('all/engineringbooks','ApiController@engineringbooks');
Route::get('all/linkstobooks','ApiController@linkstobooks');
Route::get('all/subcategories/{id}','ApiController@getsubcategories');

Route::post('add/androidaddbook','AdminController@addbook');

Route::get('getbook/content/{id}','ApiController@getcontent');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('user/download/book/{id}','AdminController@downloadfile');

Route::get('all/getlinks','ApiController@getlinks');

Route::get('all/getcomments/{id}','ApiController@allcomments');

Route::post('add/addlink','ApiController@addlink');

Route::post('user/addcomment', 'ApiController@addcomment');


//daily cake routes
Route::get('all/verses','DailyCakeApiController@verses');
Route::get('all/Testimonies','DailyCakeApiController@Testimonies');
Route::get('all/prayers','DailyCakeApiController@prayers');
Route::get('showTestimonycontent/{id}','DailyCakeApiController@showtestimony');
Route::get('my/Testimonies/{id}','DailyCakeApiController@showmytestimony');
Route::get('verse/today','DailyCakeApiController@todayverse');
Route::get('testimony/today','DailyCakeApiController@testimonytoday');
Route::get('prayer/today','DailyCakeApiController@prayertoday');

Route::post('addusername','DailyCakeApiController@addusername');

Route::post('changeusername/{id}','DailyCakeApiController@changeusername');

Route::post('adduserTestimony','DailyCakeApiController@adduserTestimony');