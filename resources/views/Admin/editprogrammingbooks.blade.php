@extends('layouts.app')

@section('content')

<div class="col-md-8 col-md-offset-2">
				<form action="{{ URL::to('update/book',$findbook->id)}}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PATCH">
					<div class="form-group">
					<label for="book">Update book</label>
						<div class="panel panel-default">
							<div class="panel-body">
                                 	<h4 class="col-md-offset-2"> update {{ $findbook->bookname }}</h4>
								<select name="subcategory_id" id="input" class="form-control" required="required">
								   @foreach($programmingcategory as $showprogrammingcategory)
									<option value="{{$showprogrammingcategory->subcategoryName}}"> {{$showprogrammingcategory->subcategoryName}}</option>
									@endforeach 
								</select>
								
									<button type="submit" class="btn btn-success pull-right ">Submit</button>
							</div>
						</div>


						
						
					</div>		
				
				
					
				</form>
			</div>

@endsection