@extends('layouts.app')
@section('content')
<div role="tabpanel">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active">
			<a href="#home" aria-controls="home" role="tab" data-toggle="tab">Programming books</a>
		</li>
		<li role="presentation">
			<a href="#engineering" aria-controls="tab" role="tab" data-toggle="tab">Engineering Books</a>
		</li>
		<li role="presentation">
			<a href="#Medicine" aria-controls="tab" role="tab" data-toggle="tab">Medicine Books</a>
		</li>
		<li role="presentation">
			<a href="#Links" aria-controls="tab" role="tab" data-toggle="tab">Links</a>
		</li>
		<li role="presentation">
			<a href="#categories" aria-controls="tab" role="tab" data-toggle="tab">categories</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home">
		<div class="col-md-12">
		<div class="col-md-7">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Category</th>
						<th>Download</th>
						<th>Delete</th>
						<th>Update</th>
					</tr>
				</thead>
				<tbody>
				@foreach($programmingcategorybooks as $showprogrammingcategorybooks)
					<tr>
						<td>{{$showprogrammingcategorybooks->bookname}}</td>
						<td>{{$showprogrammingcategorybooks->subcategory_id}}</td>
						<td><a href="{{ URL::to('user/download/book',$showprogrammingcategorybooks->id) }}"><button type="button" class="btn btn-success">Download</button></a></td>
						
                     
						<td><form action="{{ URL::to('delete/book',$showprogrammingcategorybooks->id) }}" method="POST" role="form">
			               {{ method_field('DELETE') }}
			               {{ csrf_field() }}
						<button type="submit" class="btn btn-danger">Delete</button>
					</form></td>
					


						<td>
						<a href="{{ URL::to('get/editpage',$showprogrammingcategorybooks->id) }}"><button type="button" class="btn btn-primary">update</button></a>

						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
			<div class="col-md-5 ">
				<form action="{{ URL::to('add/book')}}" method="POST" role="form" enctype="multipart/form-data">
				
					<div class="form-group">
					<label for="book">Add programming book category</label>
						<div class="panel panel-default">
							<div class="panel-body">
                                 	
								<select name="subcategoryName" id="input" class="form-control" required="required">
								@foreach($programmingcategory as $showprogrammingcategory)
									<option value="{{$showprogrammingcategory->subcategoryName}}"> {{$showprogrammingcategory->subcategoryName}}</option>
								
								@endforeach
								</select>
								
							</div>
						</div>


						<label for="book">Add programming book</label>
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="file" name="book" required="required">
							</div>
						</div>

                        <input type="hidden" name="realcategoryid" value="1">
                  <label for="picture">Add programming book picture (optional)</label>
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="file" name="picture" required="required">
							</div>
						</div>
						
					</div>		
				
					<button type="submit" class="btn btn-success ">Submit</button>
					{{ csrf_field() }}
				</form>
			</div>
		</div>

</div>

<!--second tab -->
		<div role="tabpanel" class="tab-pane" id="engineering">
			<div class="col-md-12">
		<div class="col-md-7">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Category</th>
						<th>Download</th>
						<th>Delete</th>
						<th>Update</th>
					</tr>
				</thead>
				<tbody>
					
						@foreach($engineeringcategorybooks as $showengineeringcategorybooks)
					<tr>
						<td>{{$showengineeringcategorybooks->bookname}}</td>
						<td>{{$showengineeringcategorybooks->subcategory_id}}</td>
						<td><a href=""><button type="button" class="btn btn-success">Download</button></a></td>
						

						<td><form action="{{ URL::to('delete/book',$showengineeringcategorybooks->id) }}" method="POST" role="form">
			               {{ method_field('DELETE') }}
			               {{ csrf_field() }}
						<button type="submit" class="btn btn-danger">Delete</button>
					</form></td>


						<td><a href="{{ URL::to('get/editengineering',$showengineeringcategorybooks->id) }}"><button type="button" class="btn btn-primary">update</button></a>
                                 </td>
					</tr>
					@endforeach
				</tbody>
					
				</tbody>
			</table>
		</div>
			<div class="col-md-5 ">
				<form action="{{ URL::to('add/book')}}" method="POST" role="form" enctype="multipart/form-data">
				
					<div class="form-group">
					<label for="book">Add Engineering book category</label>
						<div class="panel panel-default">
							<div class="panel-body">
							<select name="subcategoryName" id="input" class="form-control" required="required">
                                 	@foreach($engineeringcategory as $showengineeringcategory)
								
									<option value="{{$showengineeringcategory->subcategoryName}}"> {{$showengineeringcategory->subcategoryName}}</option>
								
								@endforeach
								</select>
								
							</div>
						</div>


						<label for="book">Add Engineering book</label>
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="file" name="book" required="required">
							</div>
						</div>

                         <input type="hidden" name="realcategoryid" value="3">
                  <label for="picture">Add Engineering book picture (optional)</label>
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="file" name="picture" required="required">
							</div>
						</div>
						
					</div>		
				
					<button type="submit" class="btn btn-success ">Submit</button>
					{{ csrf_field() }}
				</form>
			</div>
		</div>
		</div>
<!--Third tab -->

		<div role="tabpanel" class="tab-pane" id="Medicine">
			<div class="col-md-12">
		<div class="col-md-7">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Category</th>
						<th>Download</th>
						<th>Delete</th>
						<th>Update</th>
					</tr>
				</thead>
				<tbody>
					@foreach($mediconecategorybooks as $showmediconecategorybooks)
					<tr>
						<td>{{$showmediconecategorybooks->bookname}}</td>
						<td>{{$showmediconecategorybooks->subcategory_id}}</td>

						<td><a href="{{ URL::to('user/download/book',$showmediconecategorybooks->id) }}"><button type="button" class="btn btn-success">Download</button></a></td>

					<td><form action="{{ URL::to('delete/book',$showmediconecategorybooks->id) }}" method="POST" role="form">
			               {{ method_field('DELETE') }}
			               {{ csrf_field() }}
						<button type="submit" class="btn btn-primary">Delete</button>
					</form>
</td>
						<td><a href="{{ URL::to('get/editmedicinebook',$showmediconecategorybooks->id) }}"><button type="button" class="btn btn-danger">update</button></a>
                        </td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
			<div class="col-md-5 ">
				<form action="{{ URL::to('add/book')}}" method="POST" role="form" enctype="multipart/form-data">
				
					<div class="form-group">
					<label for="book">Add Medicine book category</label>
						<div class="panel panel-default">
							<div class="panel-body">
                                 	
								<select name="subcategoryName" id="input" class="form-control" required="required">
								   @foreach($medicinecategory as $showmedicinecategory)
									<option value="{{$showmedicinecategory->subcategoryName}}"> {{$showmedicinecategory->subcategoryName}}</option>
									@endforeach 
								</select>
								
								
							</div>
						</div>


						<label for="book">Add medicine book</label>
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="file" name="book" required="required">
							</div>
						</div>

                          <input type="hidden" name="realcategoryid" value="2">
                        <label for="picture">Add medicine book picture (optional)</label>
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="file" name="picture" required="required">
							</div>
						</div>
						
					</div>		
				
					<button type="submit" class="btn btn-success ">Submit</button>
					{{ csrf_field() }}
				</form>
			</div>
		</div>
		</div>

<!--Fourth for links tab -->
		<div role="tabpanel" class="tab-pane" id="Links">
			<div class="col-md-12">
		<div class="col-md-7">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Category</th>
						<th>Download</th>
						<th>Delete</th>
						<th>Update</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
			<div class="col-md-5 ">
				<form action="{{ URL::to('add/addlink')}}" method="POST" role="form">
				
					<div class="form-group">
					<label for="linkcategory">Add Link</label>
						<div class="panel panel-default">
							<div class="panel-body">
                                 
								<select name="linkcategory" id="input" class="form-control" required="required">
									<option value="Google Drive" id="1">Google Drive </option>
									<option value="WhatsApp" id="1"> WhatsApp</option>
									<option value="Telegram" id="1"> Telegram</option>
								</select>
								
								
							</div>
						</div>
                    

						<label for="description">Add Link description</label>
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="text" name="description" id="input" class="form-control" value="" required="required">
							</div>
						</div>


                  <label for="picture">Add Link address</label>
						<div class="panel panel-default">
							<div class="panel-body">
								<input type="text" name="linkaddress" id="input" class="form-control" value="" required="required">
							</div>
						</div>
						
					</div>		
				
					<div class="pull-center"><button type="submit" class="btn btn-primary ">Submit</button></div>
					{{ csrf_field() }}
				</form>
			</div>
		</div>
		</div>

		<!--categories -->
		<div role="tabpanel" class="tab-pane" id="categories">
			<div class="col-md-12">

	<div class="col-md-3 ">
	           <!-- add categories modal-->
	          <div class="panel panel-default">
	           	<div class="panel-body">
	           		
<a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Add category</a>
<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add category</h4>
			</div>
			<div class="modal-body">
				<form action="{{ URL::to('Admin/Addcategory') }}" method="POST" role="form">
				
					<div class="form-group">
						<label for="category">category</label>
						<input type="text" class="form-control" id="" placeholder="Input field" name="category">
					</div>
				
					
				
					<button type="submit" class="btn btn-primary">Submit</button>
					{{ csrf_field() }}
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>
	           	</div>
	           </div>
                 <!-- add subcategories -->
               <div class="panel panel-default">
	           	<div class="panel-body">

				<a class="btn btn-primary" data-toggle="modal" href='#modal-id1'>Add Subcategory</a>
				<div class="modal fade" id="modal-id1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body">
								<form action="{{ URL::to('Admin/Addsubcategory') }}" method="POST" role="form">
									<div class="form-group">
										<label for="">Add category</label>
										<input type="text" class="form-control" name="subcategory" id="" placeholder="Input field">
									</div>
									<div class="form-group">
										<select name="category" id="input" class="form-control" required="required">
										@foreach($allcategories as $showallcategories)
											<option value="{{ $showallcategories->id }}">{{ $showallcategories->categoryname}}</option>
											@endforeach
										</select>
									</div>
								
									
								
									<button type="submit" class="btn btn-primary">Submit</button>
									{{ csrf_field() }}
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary">Save changes</button>
							</div>
						</div>
					</div>
				</div>
				</div>
				</div>

			</div>
		</div>
<!--end of tabs -->

		</div>

	</div>
</div>


@endsection()