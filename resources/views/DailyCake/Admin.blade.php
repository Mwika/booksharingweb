@extends('layouts.app')

@section('content')
<div class="container ">
<div class="col-md-4 addleft">
  <form action="{{  URL::to('add/topic') }}" method="POST" role="form" class="addleftall col-md-12 ">
      <legend>Add Topic</legend>
  
      <div class="form-group">
         
          <input type="text" class="form-control" id="" placeholder="add Topic" name="topicname">
      </div>
  
      
  
      <button type="submit" class="btn btn-success">Add Topic </button>
      {{ csrf_field() }}
  </form> 
 <h4 class="col-md-offset-4 topnavtext" style="margin-top: 20px">All Topics</h4>

  <div class="lefttopics">
       <table class="table table-hover lefttopics ">
      <thead>
          <tr>
              <th>Number</th>
              <th>Name</th>
              <th>Remove</th>
          </tr>
      </thead>
      <tbody>
      @foreach($alltopics as $showalltopics)
          <tr>
              <td>{{$showalltopics->id}}</td>
              <td>{{$showalltopics->topic_name}}</td>
              <td>
              <form action="{{ URL::to('delete/topic',$showalltopics->id) }}" method="POST" role="form">  
                 {{method_field('DELETE')}}
                 {{ csrf_field() }}

                <button type="submit" class="btn btn-success pull-right">Delete</button>
              </form>
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
  </div>
 
</div>

<div class="col-md-8">
        <table class="table table-hover">
        <tbody>
            <tr>
                <td>
                <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Add Prayer</a>
                <div class="modal fade" id="modal-id">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add Prayer</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ URL::to('add/addprayer') }}" method="POST" role="form">
                                                          
                              
                               <div class="form-group">
                                  <label for="prayer from">Prayer Reference</label>
                                     <input type="text" name="prayerreference" placeholder="add prayer reference">
                                  </div>

                               <div class="form-group">
                                  <label for="prayer from">Prayer Reference</label>
                                    <textarea  id="input" class="form-control" rows="3" required="required" name="prayer"></textarea>
                                  </div>

                                
                                
                                  
                                
                                  <button type="submit" class="btn btn-primary">Submit</button>
                                    {{ csrf_field() }}
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      
                            </div>
                        </div>
                    </div>
                </div> 
                <a class="btn btn-primary" data-toggle="modal" href='#Testimony'>Add Testimony</a>
                <div class="modal fade" id="Testimony">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add Testimony</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ URL::to('add/addTestimony') }}" method="POST" role="form">
                                
                                
                                  <div class="form-group">
                                    <label for="">label</label>
                                   <textarea name="testimony" id="input" class="form-control" rows="5" required="required"></textarea>
                                  </div>
                                
                                  
                                
                                  <button type="submit" class="btn btn-primary">Submit</button>
                                    {{ csrf_field() }}
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Add verse</button>
                            </div>

                        </div>
                    </div>
                </div>

                         <a class="btn btn-primary" data-toggle="modal" href='#addverse'>Add verse</a>
                <div class="modal fade" id="addverse">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                <h4 class="modal-title">Add Verse</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ URL::to('add/addverse') }}" method="POST" role="form" >                              
                                  <div class="form-group">
                                      <input type="text"  id="input" class="form-control homeverse"  required="required" name="versefrom" >

                                    <textarea name="verse" id="input" class="form-control" rows="5" required="required"></textarea>

                                    <select name="topicid" id="input" class="form-control" required="required">
                                    @foreach($alltopics as $topics)
                                      <option value="{{$topics->id}}">{{ $topics->topic_name }}</option>
                                      @endforeach
                                    </select>


                                  </div>
                                
                                  
                                
                                  <button type="submit" class="btn btn-primary">Submit</button>
                                  {{ csrf_field() }}
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>

                </td>
            </tr>
        </tbody>
    </table>
<div class="col-md-12">
<h3 class="col-md-offset-5">Today on  Word</h3>
     <div class="panel panel-success">
        <div class="panel-body">
            <div class="col-md-4">
            <h4>Verse</h4>
            @foreach($allverses as $showallverses)
            <div class="panel  panels">
              <div class="panel-body">
              
              <h4>{{ $showallverses->topic_name }}</h4>

              <h4>{{ $showallverses->versefrom }}</h4>
              <h4>{{ $showallverses->content }}</h4>

               <form action="{{ URL::to('delete/verse',$showallverses->id) }}" method="POST" role="form">  
                 {{method_field('DELETE')}}
                 {{ csrf_field() }}

                <button type="submit" class="btn btn-success pull-right">Delete</button>
              </form>
              </div>
              
            </div>
            @endforeach
            
            </div>


            <div class="col-md-4 ">
            <h4>Testimony</h4>
             @foreach($alltestimonies as $showalltestimonies)
                <div class="panel  panels">
                  <div class="panel-body">
                  by <b>{{$showalltestimonies->name }}</b>
                 <h4> {{ $showalltestimonies->testimony  }} </h4>
                 <form action="{{ URL::to('delete/testimony',$showalltestimonies->id) }}" method="POST" role="form">  
                 {{method_field('DELETE')}}
                 {{ csrf_field() }}

                <button type="submit" class="btn btn-success pull-right">Delete</button>
              </form>
                  </div>

                </div>
                  @endforeach
            </div>
            <div class="col-md-4">
            <h4>Prayer</h4>
             @foreach($allprayers as $showallprayers)
               <div class="panel panels">
                 <div class="panel-body">
                   {{ $showallprayers->prayer }}
                  <form action="{{ URL::to('delete/prayer',$showallprayers->id) }}" method="POST" role="form">  
                 {{method_field('DELETE')}}
                 {{ csrf_field() }}

                <button type="submit" class="btn btn-success pull-right">Delete</button>
              </form>
                 </div>
               
               </div>
               @endforeach
            </div>
        </div>
    </div>
    </table>
</div>


</div>
</div>
@endsection
