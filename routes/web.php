<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'],function(){

Route::get('/home', 'HomeController@index')->name('home');
Route::post('Admin/Addcategory', 'AdminController@Addcategory');
Route::post('Admin/Addsubcategory', 'AdminController@Addsubcategory');
Route::get('admin/page','AdminController@adminpage');
Route::post('add/book','AdminController@addbook');
Route::get('all/books','ApiController@programmingbooks');

Route::post('add/addlink','ApiController@addlink');

Route::get('user/download/book/{id}','AdminController@downloadfile');

Route::get('admin/dailycake','DailyCakeController@cakeadmin');

//update books
Route::get('get/editpage/{id}', 'AdminController@editbook');
Route::get('get/editengineering/{id}', 'AdminController@editengineering');
Route::get('get/editmedicinebook/{id}', 'AdminController@editmedicinebook');

Route::patch('update/book/{id}','AdminController@updateprogrammingbook');
//delete books
Route::delete('delete/book/{id}','AdminController@deletebook');

//dailycake controller 
Route::post('add/topic','DailyCakeController@addtopic');
Route::post('add/addverse','DailyCakeController@addverse');
Route::post('add/addTestimony','DailyCakeController@addTestimony');
Route::post('add/addprayer','DailyCakeController@addprayer');

Route::delete('delete/topic/{id}','DailyCakeController@deletetopic');
Route::delete('delete/testimony/{id}','DailyCakeController@deletetestimony');

Route::delete('delete/prayer/{id}','DailyCakeController@deleteprayer');

Route::delete('delete/verse/{id}','DailyCakeController@deleteverse');

});

